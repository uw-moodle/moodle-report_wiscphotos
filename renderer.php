<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Custom render functions
 *
 * @package    report
 * @subpackage wiscphotos
 * @copyright  2013 John Hoopes
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

class report_wiscphotos_renderer extends plugin_renderer_base {

    /**
     * Wrap title in h2 tag
     *
     * @param string $title The title to display
     *
     * @return string $html The html string to echo to the page
     */
    public function rendertitle($title){

        $html = html_writer::start_tag('h2', array('class'=>'wiscphotos_title'));
        $html .= $title;
        $html .= html_writer::end_tag('h2');
        return $html;

    }

    /**
     * Render the sections screen.
     * Render a fieldset of selectable sections
     *
     * @param int $courseid The course ID to put into the section link
     *
     * @return string $html The html string to echo to the page
     */
    public function rendersections($courseid){

        global $DB, $OUTPUT;

        /* Retrieve and organize coursemap */
        $orderBy = 'term, catalog_number, section_number, subject_code';
        $dbSections = $DB->get_records('enrol_wisc_coursemap', array('courseid' => $courseid), $orderBy);
        $sections = array();
        foreach($dbSections as $dbSect){
            $isisid  = !empty($dbSect->isis_course_id) ? $dbSect->isis_course_id : $dbSect->class_number;
            $subject = !empty($dbSect->subject) ? preg_replace('/ /','', $dbSect->subject) : $dbSect->subject_code;
            $secttype = !empty($dbSect->type) ? $dbSect->type : 'SEC';
            if (!isset($sections[$dbSect->term][$isisid])) {
                $sections[$dbSect->term][$isisid] = array();
                $sections[$dbSect->term][$isisid]['subjects'] = array();
                $sections[$dbSect->term][$isisid]['catalognum'] = $dbSect->catalog_number;
            }
            $section = "$secttype $dbSect->section_number";
            $sections[$dbSect->term][$isisid]['sections'][$section][] = $dbSect->subject_code;
            $sections[$dbSect->term][$isisid]['subjects'][$dbSect->subject_code] = $subject;
        }


        $params = array('course'=>$courseid);
        $sectionurl = new moodle_url('/report/wiscphotos/index.php', $params);

        /* Display sections mapped */
        $html = '';
        if (!empty($sections)) {
            foreach($sections as $term => $courses){
                $html .= $OUTPUT->heading($this->termname($term), 3, 'termname');
                foreach($courses as $isiskey => $course){
                    //Form course name
                    $subjects   = implode('/', $course['subjects']);
                    $html .= $OUTPUT->heading("$subjects $course[catalognum]", 4, 'coursename');

                    $items = array();
                    $subjectcodes = array_keys($course['subjects']);
                    foreach($course['sections'] as $section => $sectionsubjs){
                        $item = $section;
                        $sectioninfo = explode(" " , $section);
                        $type = $sectioninfo[0];
                        $snumber = $sectioninfo[1];

                        $surl = clone($sectionurl);
                        $surl->param('isiscode', $isiskey);
                        $surl->param('stype', $type);
                        $surl->param('snumber', $snumber);

                        $link = html_writer::link($surl, $item);

                        $items[] = $link;
                    }
                    $html .= html_writer::alist($items, array('class' => 'sections'));
                }
            }
        } else {
            $html .= $OUTPUT->notification(get_string('nosections', 'report_wiscphotos'));
        }

        return $html;
    }

    /**
     * Sets up the and renders the wiscphotos table
     *
     * @param object $table The wiscphotos table
     *
     */
    public function render_wiscphotos_table( wiscphotos_table $table){

        $backtosectionshtml = '';

        $backtosectionsurl = new moodle_url('/report/wiscphotos/index.php');
        $backtosectionsurl->param('course',$table->options['courseid']);

        $backtosectionshtml .= html_writer::start_tag('div', array('class'=>'wiscphotos_backtosections'));
        $backtosectionshtml .= html_writer::link($backtosectionsurl, get_string('backtosections', 'report_wiscphotos'));
        $backtosectionshtml .= html_writer::end_tag('div');

        $numsections = $table->get_num_sections();

        if($numsections > 1){
            echo $backtosectionshtml;
        }

        $table->setup();
        $table->set_data();
        $table->finish_output();

        if (empty($table->photos)) {
            echo $this->output->notification(get_string('no_wiscphotos', 'report_wiscphotos'));
        }

        if($numsections > 1){
            echo $backtosectionshtml;
        }
    }

    /**
     * Renders the table message.
     * This message warns the user that this report is cannot be published or distributed
     *
     * @return string $html The html string to be echoed
     */
    public function rendertablemessage(){

        $html = '';
        $html .= html_writer::start_tag('div', array('class'=>'wiscphotos_table_message'));
        $html .= get_string('wiscphotos_message', 'report_wiscphotos');
        $html .= html_writer::end_tag('div');

        return $html;
    }



    function termname($term){
        $term = (string)$term;
        if (strlen($term) != 4) {
            return "Invalid term";
        }

        $century = (int)$term[0];
        $decades = (int)$term[1];
        $decyear = (int)$term[2];
        $yearsem = (int)$term[3];
        if ($century == 0) {
            $year = 1900 + $decades*10 + $decyear;
        } else {
            $year = 2000 + $decades*10 + $decyear;
        }

        switch($yearsem){
            case 2:
                $sem = "Fall";
                $year -= 1;		//This term belongs to the next academic school year
                break;
            case 3:
                $sem = "Winter";
                $year -= 1;		//This term belongs to the next academic school year
                break;
            case 4:
                $sem = "Spring";
                break;
            case 6:
                $sem = "Summer";
                break;
        }

        return $sem . ' ' . $year;
    }

}

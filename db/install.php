<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Wisc Photos report plugin installation script
 *
 * @package    report
 * @subpackage wiscphotos
 * @copyright  2013 John Hoopes
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

function xmldb_report_wiscphotos_install() {
    global $CFG, $OUTPUT;

    if (!PHPUNIT_TEST) {
        echo $OUTPUT->heading(get_string('apacheconfig', 'report_wiscphotos'), 4);

        $message  = get_string('apacheconfiginfo', 'report_wiscphotos', $CFG->dirroot);
        $infopara = html_writer::tag('p', $message, array('class' => 'centerpara'));
        echo $OUTPUT->box($infopara, 'generalbox boxaligncenter boxwidthnormal');

        $htaccess = "   ShibDisable Off

                        <Files  index.php>
                                AuthType shibboleth
                                ShibRequireSession On
                                require valid-user
                        </Files>";

        echo $OUTPUT->box($htaccess, 'generalbox boxaligncenter boxwidthnormal');
    }
}
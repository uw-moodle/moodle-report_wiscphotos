<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once($CFG->dirroot.'/report/wiscphotos/lib/wiscphotos_soapclient.php');

/**
 * A class to open a webservice connection to the wiscphotos service and then retrieve the list photo
 * links based on the list of students
 *
 * @package report_wiscphotos
 * @author  John Hoopes <hoopes@wisc.edu>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class wiscphotos_web_service {


    protected $photoclient;
    protected $timeout;

    public function __construct() {
        $this->photoclient = wiscphotos_soapclient::get();
    }


    /**
     * Gets a list of photo links and tokens from the photos webservice
     * Please see the wsdl for structure of request and response:
     * http://esb.services.wisc.edu/UWDS/WebService/photoidservice.wsdl
     *
     * @param array $students An array of students
     *
     * @return array $photos An array of objects for the photo uris and tokens
     */
    public function getPhotos($students){

        $pvis = array();
        foreach($students as $student){
            array_push($pvis, array('pvi'=>$student->person->pvi) );
        }

        $params = array(
            'personQuery' => $pvis,
            'imageAttributes'=>array(
                    'imgWidthPixels'=>100,
                    'imgHeightPixels'=>100,
                  )
            ,
        );
        try{
            $serviceresponse = $this->photoclient->GetPhotoIDURI($params);
        }catch(Exception $e){
            print_error('invalid_service_response', 'report_wiscphotos', null,  $e->getMessage());
        }
        if(!isset($serviceresponse->deliveryPhotoId)){
            print_error('invalid_service_response', 'report_wiscphotos', '');
        }

        $responses = $serviceresponse->deliveryPhotoId;

        // Create PVI keyed array of the photoToken to display in the image tag
        $photos = array();
        foreach($responses as $response){
            $pvi = $response->personQuery->pvi;
            $photos[$pvi] = $response->photoToken;
        }


        return $photos;
    }


}
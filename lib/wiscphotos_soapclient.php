<?php

/**
 * Wisc photos SOAP client
 * Singleton class for communication with the PhotosURI server
 *
 * @package    report
 * @subpackage wiscphotos
 * @copyright  2013 John Hoopes
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class wiscphotos_soapclient extends \local_wiscservices\local\soap\wss_soapclient {

    /**
     * Returns the singleton instance of this class
     *
     * @exception SoapFault|Exception on error
     * @return wisc_soapclient|null
     */
    public static function get() {
        static $instance = null; // global instance of wisc_soapclient

        if ($instance === null) {

            $config = get_config("report_wiscphotos");
            if (empty($config->photosurl)
                || empty($config->photosuser)
                || empty($config->photospass)) {
                throw new Exception("Wisc Photos not configured");
            }

            $params = array( 'exceptions' => true,
                             'trace' => true,
                             'connection_timeout' => 20,
                             'features' => SOAP_SINGLE_ELEMENT_ARRAYS,
            );

            $instance = new wiscphotos_soapclient($config->photosurl, $params);
            $instance->__setUsernameToken($config->photosuser, $config->photospass);

            $instance->__setTimeout(20);
        }
        return $instance;
    }
}
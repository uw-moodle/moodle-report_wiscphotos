<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Report API
 *
 * @package    report
 * @subpackage wiscphotos
 * @copyright  2013 John Hoopes
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once($CFG->dirroot.'/lib/tablelib.php');
require_once($CFG->dirroot.'/enrol/wisc/lib/datastore.php');

/**
 * This function extends the navigation with the report items
 *
 * @param navigation_node $navigation The navigation node to extend
 * @param stdClass $course The course to object for the report
 * @param stdClass $context The context of the course
 */
function report_wiscphotos_extend_navigation_course($navigation, $course, $context) {
    global $DB;
    if (has_capability('report/wiscphotos:view', $context)) {
        // Only display the link if there are sections in the course
        if($wiscenrolled = $DB->get_records('enrol_wisc_coursemap', array('courseid'=>$course->id, 'approved'=>1))){

            $url = new moodle_url('/report/wiscphotos/index.php', array('course'=>$course->id));
            $navigation->add(get_string('pluginname', 'report_wiscphotos'), $url, navigation_node::TYPE_SETTING, null, null, new pix_icon('i/report', ''));

        }
    }
}


/**
 * Get Sections Students based on the params
 * Because of cross connect courses and multiple isis courses in one moodle course
 *  this function will reliably take links from the section selection page and query
 *  the coursemap table to get the multiple class numbers and then get students from
 *  CHUB
 *
 * @param int $isiscode The ISIS course code
 * @param string $stype The type of section
 * @param int $snumber The section number
 *
 * @return array $students Returns array of student objects from CHUB
 */
function wiscphotos_get_sections_students($isiscode, $stype, $snumber, $courseid){
    global $DB;
    $chubds = new \enrol_wisc\local\chub\chub_datasource();

    $coursemaps = $DB->get_records('enrol_wisc_coursemap', array(
                                                            'courseid' => $courseid,
                                                            'isis_course_id'=> $isiscode,
                                                            'type'=>$stype,
                                                            'section_number'=>$snumber,
                                                            'approved' => 1,
                                                            ));

    $students = array();
    foreach($coursemaps as $isisclass){

        $classroster = $chubds->getClassRoster($isisclass->term, $isisclass->class_number);
        $classstudents = $classroster->students;
        $students = array_merge($students, $classstudents);
    }

    return $students;
}




/**
 * Extend and hold functions to create the photos table
 *
 * @package report_wiscphotos
 * @author  John Hoopes <hoopes@wisc.edu>
 * @license http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class wiscphotos_table extends flexible_table implements renderable {


    /** @var array $icons Holds an array of pix icons*/
    protected $icons;

    /** @var array $options Options for filtering in query*/
    public $options = array();

    /** @var array $chubstudents The students returned by chub for the course */
    public $chubstudents;

    /** @var array $photos The photo tokens returned by the photos service.  Keyed by pvi  */
    public $photos;

    /** @var object $wiscphotosservice The wiscphotos web service object*/
    protected $wiscphotosservice;

    /** @var array $pagestudents The students for this page */
    public $pagestudents = array();

    /**
     * Extend default constructor
     *
     * @param string $uniqid Holds the id for the table
     * @param array $chubstudents
     * @param array $options
     *
     */
    public function __construct($uniqid, $chubstudents, $options){

        /**
         * usort callback to sort by chub student lastname
         *
         * @param object $a Previous item
         * @param object $b Next Item
         *
         * @return bool
         */
        $sort_by_lastname = function($a, $b){
            return strcmp($a->person->lastName, $b->person->lastName);
        };


        $this->uniqueid = $uniqid;
        $this->chubstudents = $chubstudents;
        $this->options = $options;
        $this->wiscphotosservice = new wiscphotos_web_service();

        usort($this->chubstudents, $sort_by_lastname);

        // Call parent construct
        parent::__construct($this->uniqueid);
    }

    /**
     * Sets the data for the table.
     *
     */
    function set_data(){
        global $OUTPUT, $PAGE, $CFG;

        $courseusers = $this->get_course_users();

        // Get Num of results and add page size
        $numstudents = count($this->chubstudents);
        $this->pagesize(25, $numstudents);

        // If there are chub students process the page or no page
        if(count($this->chubstudents) > 0){
            // If under 25 students, then there is no page
            if($numstudents < $this->pagesize){
                $this->pagestudents = $this->chubstudents;
            }else{ // Meaning that we have to page students

                // If the page var isn't null, then we aren't on the first page
                if(!is_null($this->options['page'])){

                    // Get the start index that we're paging on
                    $startindex = $this->options['page'] * $this->pagesize;

                    // Next check if we're on the last page
                    if( ($startindex + $this->pagesize) > $numstudents ){ // We're on the last page
                        $this->pagestudents = array_slice($this->chubstudents, $startindex, $numstudents - $startindex);
                    }else{ // In between page
                        $this->pagestudents = array_slice($this->chubstudents, $startindex, $this->pagesize);
                    }
                }else{ // We are on the first page
                    $this->pagestudents = array_slice($this->chubstudents, 0, 25);
                }
            }

            // Next get the photos for the page students
            $this->photos = array();
            $this->photos = $this->wiscphotosservice->getPhotos($this->pagestudents);
        }

        foreach($this->pagestudents as $student){

            $firstname = $student->person->firstName;
            $lastname = $student->person->lastName;
            $pvi = $student->person->pvi;
            $netid = $student->person->netid;
            $email = $student->person->email;

            $row = array();

            $uselink = false;
            if(isset($courseusers[$pvi])){
                $uselink = true;
                // View Link
                $params = array('id'=>$courseusers[$pvi]->id);
                $viewurl = new moodle_url($CFG->wwwroot . '/user/profile.php', $params);
            }

            // set up image field.  If using view link wrap image in link
            $image = '<img src="https://photo.services.wisc.edu/delivery/loading.gif" class="phid phid_' .
                $this->photos[$pvi] . '"/>';
            if($uselink){
                $imagehtml = html_writer::link($viewurl, $image);
            }else{
                $imagehtml = $image;
            }
            $row[] = $imagehtml;

            // set up name field.  If using view link wrap name text in link
            $nametext = $lastname . ', ' . $firstname;
            if($uselink){
                $namehtml = html_writer::link($viewurl, $nametext);
            }else{
                $namehtml = $nametext;
            }
            $row[] = $namehtml;
            $row[] = $email;

            $this->add_data($row);
        }
    }

    /**
     * Do initial setup for this specific table
     *
     * @param object $baseurl A moodle_url object
     *
     * @return object $parent parent return
     */
    public function setup($baseurl = null){
        global $PAGE;

        if (!isset($baseurl)) {
            $baseurl = $PAGE->url;
        }
        $this->define_baseurl($baseurl);

        $this->set_attribute('id', $this->uniqueid);
        $this->set_attribute('class', 'generaltable wiscphotos_view_table');
        $this->set_attribute('cellspacing', '0');

        $columns = array(
            'picture'  => get_string('wiscphotos_picture_title', 'report_wiscphotos'),
            'name'     => get_string('wiscphotos_name', 'report_wiscphotos'),
            'email'    => get_string('wiscphotos_email', 'report_wiscphotos'),
        );

        $this->define_columns(array_keys($columns));
        $this->define_headers(array_values($columns));

        foreach(array_keys($columns) as $column){
            $this->column_class($column, 'col_'.$column);
        }

        $this->is_downloadable(false);

        parent::setup();
    }


    /**
     * Get the a list of users for the specified course
     *
     * @return array $users array of user objects for the course
     */
     protected function get_course_users(){

        $ccontext = context_course::instance($this->options['courseid']);
        $users = get_enrolled_users($ccontext, null, null, 'u.id,u.idnumber');

        //create users array keyed by pvi number
        $pviusers = array(); // users array keyed by pvi
        foreach($users as $user){
            $pviusers[$user->idnumber] = $user;
        }

        return $pviusers;
    }


    /**
     * Gets the number of sections to determine if we need to display back to sections link
     *
     * @return int $numsections
     */
    public function get_num_sections(){
        global $DB;

        $coursemaps = $DB->get_records('enrol_wisc_coursemap', array('courseid'=> $this->options['courseid']));

        return count($coursemaps);

    }


}






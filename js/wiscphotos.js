(function (c) {
    function a() {
        c.getJSON("https://photo.services.wisc.edu/delivery/session_poll?callback=?", d);
    }

    function d(e) {
        if (e && e.ready) {
            b();
        } else {
            setTimeout(a, 200);
        }
    }

    function b() {
        var e = "https://photo.services.wisc.edu/delivery/i/p/";
        c("img.phid").each(function (g, h) {
            var f = c(h).attr("class").split(/\s+/);
            c.each(f, function (k, l) {
                if (l.indexOf("phid_") != -1) {
                    var i = l.substring(5, l.length);
                    c(h).attr("src", e + i + ".png");
                }
            });
        });
    }
    c(document).ready(function () {
        var e = c("<iframe></iframe>");
        e.attr("src", "https://photo.services.wisc.edu/delivery/session_check");
        e.css({
            position: "absolute",
            left: "-4000px"
        });
        e.appendTo("body");
        a();
    });
})(jQuery);
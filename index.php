<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * The report page
 *
 * @package    report
 * @subpackage wiscphotos
 * @copyright  2013 John Hoopes
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


require_once('../../config.php');
require_once($CFG->dirroot.'/report/wiscphotos/lib.php');
require_once($CFG->dirroot.'/report/wiscphotos/lib/wiscphotos.php');


$courseid       = required_param('course', PARAM_INT);
$isiscode       = optional_param('isiscode', null, PARAM_TEXT);
$stype          = optional_param('stype', null, PARAM_TEXT);
$snumber        = optional_param('snumber', null, PARAM_TEXT);
$page           = optional_param('page', null, PARAM_INT);

$params = array();
$params['course'] = $courseid;
$params['isiscode'] = $isiscode;
$params['stype'] = $stype;
$params['snumber'] = $snumber;

$PAGE->set_url('/report/wiscphotos/index.php', $params);
$PAGE->set_pagelayout('report');

// Get course and context information and make sure the user is able to access them
$course = $DB->get_record('course', array('id'=>$courseid), '*', MUST_EXIST);
$context = context_course::instance($course->id);
require_login($course);
require_capability('report/wiscphotos:view', $context);

add_to_log($course->id, "course", "report wiscphotos", "report/wiscphotos/index.php?id=$course->id", $course->id);

$renderer = $PAGE->get_renderer('report_wiscphotos');
// require jquery
$PAGE->requires->jquery();

if(!is_null($isiscode) && !is_null($stype) && !is_null($snumber)){

    /*if(!isset($SESSION->wiscphotos)){
        $SESSION->wiscphotos = new stdClass();
        $SESSION->wiscphotos->urlparams = array();
    }

    if($SESSION->wiscphotos->urlparams == $params){
        $students = $SESSION->wiscphotos->students;



    }else{*/
        $students = wiscphotos_get_sections_students($isiscode, $stype, $snumber, $course->id);
        //$SESSION->wiscphotos->urlparams = $params;
        //$SESSION->wiscphotos->students = $students;
    //}

    $photostable = new wiscphotos_table('wiscphotos_table', $students, array('courseid' => $course->id,
                                                                             'page'     => $page,
                                                                             'stype'    => $stype,
                                                                             'snumber'  => $snumber,
                                                                             'isiscode' => $isiscode) );

    // Setup page
    $PAGE->set_pagelayout('report');
    $PAGE->set_title(get_string('pluginname', 'report_wiscphotos'));
    $PAGE->set_heading($PAGE->title);
    $PAGE->requires->js('/report/wiscphotos/js/sizzle.js');
    $PAGE->requires->js('/report/wiscphotos/js/wiscphotos.js');

    // Display page
    $title = $stype . ' ' . $snumber . ' - ' . get_string('title', 'report_wiscphotos');
    $PAGE->set_title($title);
    $PAGE->set_heading($title);

    echo $OUTPUT->header();
    echo $renderer->rendertitle($title);
    echo $renderer->rendertablemessage();
    $renderer->render($photostable);
    echo $OUTPUT->footer();



}else{

    // Get sections and render them.  If there is only one section automatically redirect.

    $sections = $DB->get_records('enrol_wisc_coursemap', array('courseid'=>$courseid), 'section_number' );
    if(count($sections) == 1){
        foreach($sections as $section){
            $params = array(
                'course'    => $course->id,
                'isiscode'  => $section->isis_course_id,
                'stype'     => $section->type,
                'snumber'   => $section->section_number,
            );

            $redirecturl = new moodle_url($CFG->wwwroot.'/report/wiscphotos/index.php',$params);
            redirect($redirecturl, null, 0);
            exit();
        }
    }

    $title = get_string('title', 'report_wiscphotos');
    $PAGE->set_title($title);
    $PAGE->set_heading($title);

    echo $OUTPUT->header();
    echo $OUTPUT->heading($title);
    echo $renderer->rendersections($courseid);

    echo $OUTPUT->footer();
}




Please add this to the .htaccess file in production in order to require Shib logins for this report.

-------

ShibDisable Off

<Files  index.php>
        AuthType shibboleth
        ShibRequireSession On
        require valid-user
</Files>


-------
<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Settings
 *
 * @package    report
 * @subpackage wiscphotos
 * @copyright  2013 John Hoopes
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/course/lib.php');
require_once($CFG->dirroot.'/local/wiscservices/locallib.php');
require_once($CFG->libdir.'/authlib.php');

$modname = 'report_wiscphotos';
$plugin = 'report/wiscphotos';

if ($hassiteconfig) {
    $settings = new admin_settingpage($modname, get_string('pluginname', 'report_wiscphotos'));
    //$ADMIN->add('reports', $settings);

    //--- heading ---
    $settings->add(new admin_setting_heading('report_wiscphotos', '', get_string('pluginname_desc', 'report_wiscphotos')));

    if (!class_exists('SoapClient')) {
        $settings->add(new admin_setting_heading('enrol_phpsoap_noextension', '', get_string('phpsoap_noextension', 'report_wiscphotos')));
    } else {

        $settings->add(new admin_setting_heading('report_wiscphotos_service_settings', get_string('photosserver_settings', 'report_wiscphotos'), ''));
        $settings->add(new admin_setting_configtext('report_wiscphotos/photosurl', get_string('wiscphotosurl_key', 'report_wiscphotos'), '', 'http://esb.services.wisc.edu/UWDS/WebService/photoidservice.wsdl', PARAM_URL,60));
        $settings->add(new admin_setting_configtext('report_wiscphotos/photosuser', get_string('wiscphotosuser_key', 'report_wiscphotos'), '', 'CAE_MOODLE_PHOTO_SERVICE_USR', PARAM_RAW));
        $settings->add(new admin_setting_configpasswordunmask('report_wiscphotos/photospass', get_string('wiscphotospass_key', 'report_wiscphotos'), '', '', PARAM_RAW));






    }
}